//Liangting Chen April 10 hw08
//
//
import java.util.Scanner; 
public class RemoveElement{
  public static int[] randomInput(int num[]) {
    int i = 0;
    for (i = 0; i < num.length; i++) {     
      num[i] = (int) (Math.random() * 10); 
    }
    return num; 
  }
  
  public static int[] delete(int[] list, int pos) { 
    
    int[] newList = new int[9]; 
    int i = 0;
    
    for (i = 0; i < list.length; i++) { 
      if (i < pos) { 
      newList[i] = list[i];
      }
else if (i > pos) {
      newList[i - 1] = list[i];
      }
    }
    return newList;
  }
   public static int[] remove(int[] list, int target) {
    int i = 0;
    int newLength = 10; 
    int j = 0;  
    for (i = 0; i < list.length; i++) {
      if (list[i] == target) { 
        newLength--;
      }
    }
    
    int[] newList = new int[newLength];
for (i = 0, j = 0; i < list.length; i++) {
      if (list[i] == target) { 
        continue; 
      }
      else {
        newList[j] = list[i]; 
        j++;
      }
    }
    return newList;}
    
  
  public static void main(String [] arg){
 Scanner scan = new Scanner(System.in);
int num[] = new int[10]; 
int[] newArray1={}; 
int[] newArray2={}; 
int index,target;
int i = 0;
  String out1;
 String answer="";
 
    do{
   System.out.print("Random input 10 ints [0-9]"); 
   num = randomInput(num); 
   String out = "The original array is:";
   out += listArray(num);
   System.out.println(out);
   System.out.print("Enter the index ");
   index = scan.nextInt();
    
   if (index >= 0 && index <= 9) {
   newArray1 = delete(num,index); 
    System.out.println("Index " + index + " element is removed");
    out1="The output is ";
    out1+=listArray(newArray1); 
    System.out.println(out1);
    }
    else {
      newArray1 = num;
      System.out.println("The index is not valid.");
      out1="The output is ";
      out1+=listArray(newArray1);  
      System.out.println(out1);
    }
   System.out.print("Enter the target value ");
   target = scan.nextInt();
   
    for (i = 0; i < num.length; i++) {
      if (num[i] == target) { 
        System.out.println("Element " + target + " has been removed");
        String out2="The output is ";
        out2+=listArray(newArray2); 
        System.out.println(out2);
        break;
      }
      if (num[i] != target) {
        if (i == num.length - 1) {
        System.out.println("Element " + target + " cannot be not found."); 
        String out2="The output is ";
        out2+=listArray(newArray1); 
        System.out.println(out2);
 }
        continue;
      }
    }
    
   answer=scan.next(); 
 }
    while(answer.equals("Y") || answer.equals("y"));
  }
  
  public static String listArray(int num[]){ 
 String out="{";
 for(int j=0;j<num.length;j++){
   if(j>0){
     out+=", ";
   }
   out+=num[j];
 }
 out+="} ";
 return out;
 }
}