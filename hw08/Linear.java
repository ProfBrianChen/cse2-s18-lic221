//Liangting Chen April 8 hw08 part1
import java.util.Random;  
import java.util.Scanner;
public class Linear{
  public static String search(int[] students, int gradeEntered){  
    int low = 0; 
    int high = 14; 
    int count = 0; 
    while (high >= low) { // 
      count++; 
int a = Math.round((low + high)/2); 
      if (students[a] == gradeEntered) {  
        return "found in the list with " + count + " iterations";
      }
        else if (students[a] > gradeEntered) { 
          high = a - 1; 
        }
          else if (students[a] < gradeEntered) { 
            low = a + 1;
          }
    }   
return "not found in the list with " + count + " iterations";
  }
    
  
  public static String LinearSearch(int[] students, int gradeEntered){ 
    int i = 0; 
    int count = 0; 
  for (i = 0; i < students.length; i++) {
    count++; 
    if (students[i] == gradeEntered) { 
    return "found in the list with " + count + " iterations";
    }
  }
 return "not found in the list with " + count + " iterations";
  }
    
  
  public static void Scramble(int[] students) { 
  
    Random scramble = new Random(); 
    int i = 0;
    for (i = 0; i < students.length; i++) { 
      
      int target = (int) ( students.length * Math.random() );
      
      int swap = students[target]; 
        students[target] = students[i];
        students[i] = swap;
    }
for (i = 0; i < 15; i++) { 
      System.out.print(students[i] + " ");
    }
    
  }
    
  
  public static void main(String[] args) {
    
    Scanner myScanner = new Scanner(System.in);
    int i = 0;
    
    int[] students = new int[15];
System.out.println("Enter 15 ascending final grades in the course: ");
    for (i = 0; i < 15; i++) {
      
      while (!myScanner.hasNextInt()) {
        System.out.println("Error");
        System.out.println("enter an integer, enter the value again: ");
        myScanner.next();
        continue;
      }
      students[i] = myScanner.nextInt();
     
      while (students[i] > 100 || students[i] < 0) {
        System.out.println("enter an integer between 0 to 100: ");
        students[i] = myScanner.nextInt();
        continue;
      }
      
      if (i >= 1) {
        while (students[i] < students[i - 1]) {
          System.out.println("enter an integer greater or equal to the last one: ");
          students[i] = myScanner.nextInt();
          continue;
        } 
      
      }
      
    }
  
    for (i = 0; i < 15; i++) {
      System.out.print(students[i] + " ");
    }
    System.out.println();
    System.out.print("Enter the grade that you want! ");
    int gradeEntered = myScanner.nextInt();
System.out.println(gradeEntered + " is " + search(students, gradeEntered) );
    System.out.println("Scrambled: ");
    Scramble(students);
    System.out.println();
    System.out.println("Enter the grade that you want! ");
    gradeEntered = myScanner.nextInt();
    System.out.println(gradeEntered + " is " + LinearSearch(students, gradeEntered) );
   
  }
}