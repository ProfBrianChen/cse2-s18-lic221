//
public class Cyclometer{
    //main method required for every Java program
    public static void main(String[]args) {
//our input data.

int secsTrip1=480;  //the number of minutes for the trip
int secsTrip2=3220; //the number of minutes for the trip
int countsTrip1=1561; //the number of counts for the trip
int countsTrip2=9037; //the number of counts for the trip

double wheelDiameter=27.0,  //the diameter of the wheel
PI=3.14159, // PI
feetPerMile=5280, // how many feet are there in a Mile
inchesPerFoot=12, // how many inches are there in a Foot
secondsPerMinute=60;  //how many inches are there in a minute
double distanceTrip1, distanceTrip2,totalDistance;  // distnace of the trip
System.out.println("Trip 1 took "+
       	     (secsTrip1/secondsPerMinute)+" minutes and had "+
       	      countsTrip1+" counts.");//print the seconds of the trip
System.out.println("Trip 2 took "+
       	     (secsTrip2/secondsPerMinute)+" minutes and had "+
       	      countsTrip2+" counts.");//print the seconds of the trip
//
//
distanceTrip1=countsTrip1*wheelDiameter*PI;
    	// Above gives distance in inches
    	//(for each count, a rotation of the wheel travels
    	//the diameter in inches times PI)
	distanceTrip1/=inchesPerFoot*feetPerMile; // Gives distance in miles
	distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;
	totalDistance=distanceTrip1+distanceTrip2;

//Print out the output data.
  System.out.println("Trip 1 was "+distanceTrip1+" miles");//print the distance of the trip
	System.out.println("Trip 2 was "+distanceTrip2+" miles");//print the distance of the trip
	System.out.println("The total distance was "+totalDistance+" miles");//print the distance of the trip

   
    } //end of main method 
}// end of class



 

	
