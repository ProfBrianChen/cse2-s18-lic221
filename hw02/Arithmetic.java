//Liangting Chen, Feb,4th,2018,hw2,Arithmetic
//
public class Arithmetic {
  //main method required for every Java program
  public static void main(String[] args) {
  //our input data

//Number of pairs of pants
int numPants = 3;
//Cost per pair of pants
double pantsPrice = 34.98;

//Number of sweatshirts
int numShirts = 2;
//Cost per shirt
double shirtPrice = 24.99;

//Number of belts
int numBelts = 1;
//cost per box of envelopes
double beltCost = 33.99;

//the tax rate
double paSalesTax = 0.06;

double totalCostOfPants; //total cost of pants
    totalCostOfPants=numPants*pantsPrice;
    
double totalCostOfShirts; //total cost of shirts
    totalCostOfShirts=numShirts*shirtPrice;
    
double totalCostOfBelts; //total cost of Belts
    totalCostOfBelts=numBelts*beltCost;
    
double taxOfPants; //total cost of shirts
    taxOfPants=numPants*pantsPrice*paSalesTax;
    
double taxOfShirts; //total cost of shirts
    taxOfShirts=numShirts*shirtPrice*paSalesTax;
    
double taxOfBelts; //total cost of shirts
    taxOfBelts=numBelts*beltCost*paSalesTax;
    
double totalCostBeforeTax; //total cost before tax
    totalCostBeforeTax=totalCostOfPants+totalCostOfBelts+totalCostOfShirts;
    
double totalTax;//total tax
    totalTax=taxOfPants+taxOfShirts+taxOfBelts;
    
double totalCostOfPurchase;//total cost of purchase
    totalCostOfPurchase=totalCostBeforeTax+totalTax;

System.out.println("total cost of pants is"+String.format("%.2f",totalCostOfPants));
System.out.println("total cost of shirts is"+String.format("%.2f",totalCostOfShirts));
System.out.println("total cost of belts is"+String.format("%.2f",totalCostOfBelts));
System.out.println("tax of pants is"+String.format("%.2f",taxOfPants));
System.out.println("tax of shirts is"+String.format("%.2f",taxOfShirts));
System.out.println("tax of belts is"+String.format("%.2f",taxOfBelts));
System.out.println("total cost before tax is"+String.format("%.2f",totalCostBeforeTax));
System.out.println("total tax is"+String.format("%.2f",totalTax));
System.out.println("total cost of purchase is"+String.format("%.2f",totalCostOfPurchase));
  }
}



    
      