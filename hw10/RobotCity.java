//
//
//
import java.util.Random;
public class RobotCity{
  public static int[][] buildCity(){
    Random rand = new Random();
    int length = rand.nextInt(6)+10;
    int width = rand.nextInt(6)+10;
    int [] [] size = new int [length] [width];
    for(int x = 0; x < length; x++){
      for(int y = 0; y < width; y++){
        size[x][y] = rand.nextInt(900)+100;
      }  
    }
    return size;
  }

  public static void display(int[][] city){
    for (int row = 0; row < city.length; row ++){
      for (int col = 0; col < city[0].length; col++){
          System.out.printf("% 3d", city[row][col]);
      }
      System.out.printf("\n");
    }
  }
  
  public static int[] [] invade(int[][] city, int k){
    Random rand = new Random();
    for (int i = 0; i < k; i++) {
        int q = rand.nextInt(city.length);
        int j = rand.nextInt(city[0].length);
        while (city[i][j] < 0) {
            q = rand.nextInt(city.length);
            j = rand.nextInt(city[0].length);
        }
        city[q][j] = -city[q][j];
    }
    return city;
  }

  public static int[][] update(int[][]city){
    for (int i = 0; i < city.length; i++){
      for (int j =0; j<city[0].length;j++){
      if(city[i][j] < 0){
        city[i][j+1]=-city[i][j+1];
        if(i==city.length||j==city[0].length-1||i==city.length-1){}
       }
      }
    }
    return city;
  }
  
  public static void main(String args[]){
    Random rand = new Random();
    int robot = rand.nextInt(100);
    int[][] city = buildCity();
    display(city);
  //  display(invade(city,robot));
    display(update(city));
    for(int i = 0; i<5;i++){
      display(update(city));
    }
  }
}