//Liangting Chen, Feb,13th,2018,hw3,Pyramid
//
//
import java.util.Scanner;//import statement
public class Pyramid{
    			// main method required for every Java program
   			public static void main(String[] args) {
 Scanner myScanner = new Scanner( System.in );//declare an instance
           System.out.print("Enter the square side of the pyramid: ");//let the user enter the length
  double lengthOfSquare = myScanner.nextDouble();//accept user input
          System.out.print("Enter the height of the pyramid: ");//prompt the user enter the height
  double heightOfPyramid = myScanner.nextDouble();//accept user input
     double volume;       
 volume=lengthOfSquare*lengthOfSquare*heightOfPyramid/3;//the formula of the volume of the Pyramid
          System.out.println("The volume of the pyramid is" + volume);
}//end of the main method
  }//end of the class